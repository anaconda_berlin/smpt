package de.vl.vs.smpt;

public class ReadData extends SessionState {

	public ReadData(MailSession session) {
		super(session);
	}
	
	@Override
	protected String[] parseLine(String line) {
		return new String[] {"", line};
	}

	@Override
	protected SessionState processLine(String cmd, String data) {
		if( data.equals(".")) {
			session.reply("250 mail received");
			session.finishMail();
			return new AwaitHelo(session);
		}
		else {
			session.reply("354 waiting for more data. end with <CRLF>.<CRLF>");
			session.addDataLine(data);
			return this;
		}
	}

}
