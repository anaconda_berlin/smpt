package de.vl.vs.smpt;

import java.util.ArrayList;
import java.util.List;

public class MailSession {

	private SessionState state;

	private String clientName;
	private String mailFrom;
	private String rcptTo;
	private List<String> data = new ArrayList<>();

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getMailFrom() {
		return mailFrom;
	}

	public void setMailFrom(String mailFrom) {
		this.mailFrom = mailFrom;
	}

	public String getRcptTo() {
		return rcptTo;
	}

	public void setRcptTo(String rcptTo) {
		this.rcptTo = rcptTo;
	}

	public List<String> getData() {
		return data;
	}

	public void addDataLine(String data) {
		this.data.add(data);
	}

	
	public MailSession() {
		reply("220 test mail service ready");
		state = new AwaitHelo(this);
	}
	
	public void nextLine(String nextLine) {
		state = state.nextLine(nextLine);
	}
	

	public void reply(String line) {
		System.out.println(line);
	}
	
	public void quit() {
		System.out.println("Good bye!");
	}
	
	public void finishMail() {
		System.out.println("write mail to text file");
		StringBuilder sb = new StringBuilder();
		for( String s: data) {
			sb.append(s).append("\\r\\n");
		}
		System.out.println("client: " + clientName + ", mailFrom: " + mailFrom + ", rcptTo: " + rcptTo + ", data: " + sb.toString());
	}

}
