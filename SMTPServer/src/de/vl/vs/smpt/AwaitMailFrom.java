package de.vl.vs.smpt;

public class AwaitMailFrom extends SessionState {

	public AwaitMailFrom(MailSession session) {
		super(session);
	}

	@Override
	protected SessionState processLine(String cmd, String data) {
		if( cmd.equals("MAIL FROM")) {
			session.setMailFrom(data);
			session.reply("250 OK");
			return new AwaitRcptTo(session);
		}
		else {
			session.reply("500 " + cmd);
			return this;
		}
	}

}
