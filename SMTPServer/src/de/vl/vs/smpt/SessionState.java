package de.vl.vs.smpt;

public abstract class SessionState {
	
	protected MailSession session;
	
	public SessionState(MailSession session) {
		this.session = session;
	}
	
	public SessionState nextLine(String line) {
		String[] parsed = parseLine(line);
		String cmd = parsed[0].toUpperCase();
		String data = parsed[1];
		if( cmd.equals("HELP")) {
			help(data);
			return this;
		}
		else if(cmd.equals("NOOP")) {
			session.reply("250 OK");
			return this;
		}
		else if(cmd.equals("QUIT")) {
			session.reply("221 OK");
			session.quit();
			return null;
		}
		else  {
			return processLine(cmd, data);
		}
	}
	
	protected String[] parseLine(String line) {
		int col = line.indexOf(':');
		int space = line.indexOf(' ');
		int sep = col > 0 ? col: space;
		if( sep > 0 ) {
			return new String[]{line.substring(0, sep), line.substring(sep+1)};
		}
		else  {
			return new String[]{line, null};
		}
	}
	
	protected abstract SessionState processLine(String cmd, String data);
		
	protected void help(String data) {
		session.reply("214 help on " + data);
	}

}
