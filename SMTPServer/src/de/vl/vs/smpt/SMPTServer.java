package de.vl.vs.smpt;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.Iterator;
import java.util.Set;

/**
 * 
 * 
 * @author Ana
 *
 */
public class SMPTServer {

	public final static int PORT = 12345;

	public static void main(String[] args) {

		System.out.println("Server starts listenneing to port " + PORT);
		// create new selector
		Selector selector;
		try {
			selector = Selector.open();

			ServerSocketChannel servSock = ServerSocketChannel.open();
			// before channel is registered with the selector it must be in non blocking
			// mode
			servSock.configureBlocking(false);
			servSock.socket().bind(new InetSocketAddress(PORT));
			System.out.println("selector bound to socket channel");
			// accept connection from client
			servSock.register(selector, SelectionKey.OP_ACCEPT);

			Charset messageCharset = Charset.forName("US-ASCII");

			while (true) {
				if (selector.select() == 0)/* blocking */
					continue;
				try {
					Set<SelectionKey> selectedKeys = selector.selectedKeys();

					Iterator<SelectionKey> iter = selectedKeys.iterator();
					while (iter.hasNext()) {
						SelectionKey key = iter.next();
						if (key.isAcceptable()) {
							System.out.println("Accept connection from client");
							ServerSocketChannel sock = (ServerSocketChannel) key.channel();
							SocketChannel client = sock.accept();
							client.configureBlocking(false);
							// reregister socket for reading or writing
							client.register(selector, SelectionKey.OP_READ, SelectionKey.OP_WRITE);

							CharBuffer c = CharBuffer.wrap("220 Hello from server!\r\n");
							System.out.println("writing " + c);
							ByteBuffer b = messageCharset.encode(c);
							// b.flip();
							int a = 0;
							while (b.hasRemaining()) {
								a += client.write(b);
							}
							System.out.println("wrote " + a + " bytes");

						}
						if (key.isReadable()) {
							System.out.println("Read command");
							ByteBuffer buf = ByteBuffer.allocate(1024);
							SocketChannel channel = (SocketChannel) key.channel();
							channel.read(buf);
							buf.flip();

							CharsetDecoder decoder = messageCharset.newDecoder();
							CharBuffer charBuf = decoder.decode(buf);
							String s = charBuf.toString();
							System.out.println("String = " + s);
							// TODO parse lines

							writeFile(charBuf);

							// reregister socket for reading or writing
							channel.register(selector, SelectionKey.OP_READ, SelectionKey.OP_WRITE);

						}
						if (key.isWritable()) {
							System.out.println("Write command");
						}

						// Remove it from the list to indicate that it is being processed
						iter.remove();
					}
				} catch (UnsupportedOperationException e) {
					// TODO: handle exception
				}
			}
		} catch (IOException e1) {
			System.out.println("Error in reading from channel");
			System.out.println(e1.getMessage());
			System.exit(1);
		}

	}

	private static void writeFile(CharBuffer charBuf) throws IOException {
		// TODO question: also use selector here for FileChannel?
		RandomAccessFile aFile;
		try {
			aFile = new RandomAccessFile(constructFilePath(), "rw");
			aFile.seek(aFile.length());

			FileChannel inChannel = aFile.getChannel();

			ByteBuffer buf = ByteBuffer.allocate(48);
			buf.clear();
			buf.put(charBuf.toString().getBytes());

			buf.flip();

			while (buf.hasRemaining()) {
				try {
					inChannel.write(buf);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static String constructFilePath() {
		// TODO generate path as given in uebung instruction as read from parser
		return "mail.txt";
	}

	// Questione:
	// write files: FileChannels are blocking=>?

	// byte[] data = response.getBytes("UTF-8");
	// ByteBuffer buffer = ByteBuffer.wrap(data);
	// while (buffer.hasRemaining())
	// clientChannel.write(buffer);
	// clientChannel.close();
	// }

}
