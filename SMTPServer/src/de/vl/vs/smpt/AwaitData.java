package de.vl.vs.smpt;

public class AwaitData extends SessionState {

	public AwaitData(MailSession session) {
		super(session);
	}

	@Override
	protected SessionState processLine(String cmd, String data) {
		if( cmd.equals("DATA")) {
			return new ReadData(session);
		}
		else {
			session.reply("500 " + cmd);
			return this;
		}
	}

}
