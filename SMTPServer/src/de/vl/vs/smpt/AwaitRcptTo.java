package de.vl.vs.smpt;

public class AwaitRcptTo extends SessionState {

	public AwaitRcptTo(MailSession session) {
		super(session);
	}

	@Override
	protected SessionState processLine(String cmd, String data) {
		if( cmd.equals("RCPT TO")) {
			session.setRcptTo(data);
			session.reply("250 OK");
			return new AwaitData(session);
		}
		else {
			session.reply("500 " + cmd);
			return this;
		}
	}

}
