package de.vl.vs.smpt;

public class AwaitHelo extends SessionState {

	public AwaitHelo(MailSession session) {
		super(session);
	}

	@Override
	protected SessionState processLine(String cmd, String data) {
		if( cmd.equals("HELO")) {
			session.setClientName(data);
			session.setMailFrom(null);
			session.setRcptTo(null);
			session.getData().clear();
			session.reply("250 OK");
			return new AwaitMailFrom(session);
		}
		else {
			session.reply("500 " + cmd);
			return this;
		}
	}

}
